[TOC]
## 获取框架对象
```
//参数为空则能获取初始化框架信息
var avFramework = av.framework();
```
## 框架对象参数
| 键 | 名称 | 类型 | 描述 |
| --- | --- | --- | --- |
| config | 公共配置 | Object| 返回框架配置信息。注意，返回值是无法进行配置真实修改的 |
| event | 公共事件 | Object | 返回公共事件列表。同时存在工程事件的时候，先执行公共事件，再执行工程事件。注意，返回值是无法进行真实修改的  |
| page | 页面列表 | Object | 返回页面地址列表。键名称是页面地址，值是工程ID与工程文件地址。注意，返回值是无法进行真实修改的 |
| project | 工程列表 | Object |  返回所有加载的工程对象列表。键名称是工程ID，值是工程对象。注意，返回值是无法进行真实修改的，当然可以对某一个工程对象进行修改 |
| history | 浏览记录 | Object | 浏览历史的操作。注意，返回值是无法进行真实修改的 |
|  ready | 页面准备好后才执行  | Function  |  传入一个函数，当页面准备好后才执行。注意，这个不是事件，只是一个执行队列 |
| export | 获取/设置导出文件的数据 | Function  | 参数为空，返回导出对象数据，键名称是文件地址，值是文件内容。注意，返回值是无法进行真实修改的。如果传了一个参数，是获取这个键的值。如果传了两个参数，那么就是设置这个键值数据  |
| run| 运行框架 | Function| 运行框架的操作 |

## 设置公共配置
```
av.framework({debug:true})
```

## 框架运行控制

```
//以默认方式运行
av.framework().run();
```

## 浏览记录操作
```
//获取浏览记录的对象
av.framework().history;
```
| 方法名称 |  描述 |
| --- | --- | 
| pageBack | 加载页面的上一页| 
| pageForward | 加载页面的下一页| 
| pageRemove | 删除当前页面浏览记录 | 
| pageBackList | 获取页面的上翻列表 | 
| pageForwardList | 获取页面的下翻列表 | 
| pageBackExist| 判断上一页面是否存在 | 
| pageForwardExist | 判断下一页面是否存在 | 
| routerBack | 加载锚点路由变化的上一页| 
| routerForward | 加载锚点路由的下一页| 
| routerRemove | 删除当前锚点路由浏览记录 | 
| routerBackList | 获取锚点路由的上翻列表 | 
| routerForwardList | 获取锚点路由的下翻列表 | 
| routerBackExist| 判断上一个锚点路由是否存在 | 
| routerForwardExist | 判断下一个锚点路由是否存在 | 

## export 导出文件数据操作
获取全部的导出数据：
```
av.framework().export();
```
获取`test.josn`导出数据：
```
av.framework().export("test.josn");
```
设置`test.josn`导出数据，必须是一个数字或字符串：
```
av.framework().export("test.josn","这是测试");
```
删除`test.josn`导出数据，第二个参数传入`undefined`、`false`、`null`：
```
av.framework().export("test.josn",undefined);
```





