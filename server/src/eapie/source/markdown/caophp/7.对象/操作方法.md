<blockquote class="info"><p>对象操作存在的目的是可以单列模式下预实例化对象。还能查看对象第一次实例化位置和一些实例化状态，方便了实例化的管理和调试。</p></blockquote>

##### 全局赋值函数：
~~~
object([mixed $identify][,string $class_name][,array $construct_args][,object $clone_object][,bool $coverage][,closure $closure]);
~~~
##### 继承框架类：
~~~
framework\cao::object([mixed $identify][,string $class_name][,array $construct_args][,object $clone_object][,bool $coverage][,closure $closure]);
~~~

| 参数类型  |  参数名称  |  参数备注  |  是否必须  |  传参顺序 |
| --- | --- | --- | --- | --- |
|  string/int/float  |  $identify  |  标识  |  否  |  mixed[0]  |
|  string  |  $class_name  |  类名称  |  否  |  mixed[1]  |
|  array  |  $construct_args  |  构造方法的参数  |  否  | array[0] |
|  object  |  $clone_object  |  复制原对象  |  否  | object[0]  |
|  bool  |  $coverage  |  是否覆盖已存在标识  |  否  | bool[0]  |
|  closure  |  $closure  |  闭包函数  |  否  | closure[0] |

- $identify 对象实例化的标识，可用标识找到以实例化对象进行复用。
- $class_name 将被实例化的类名(包含命名空间)。
- $construct_args 在第一次实例化的时候，给类的构造函数传入的参数，如array(参数1,参数2,...)，是一个索引数组。
- $clone_object 原对象。将这个对象复制给当前的标识。如果标识已经存在对象，则需要$coverage为true，进行覆盖。否则复制无效。
- $coverage 为true覆盖则覆盖已存在标识，默认false则不覆盖。
- $closure 放入一个参数，如function($object){}，$object就是实例化后的对象。