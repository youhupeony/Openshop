<blockquote class="info"><p>cao.php 为了让项目更好的管理，采用扁平化设计，目录结构更加清晰，让程序维护更加轻松。</p></blockquote>

~~~
├─ localhost			网站根目录 //开发者手动创建，目录名称自定义
	├─ application		应用模块目录	//开发者手动创建，目录名称自定义
	├─ cache			运行缓存目录	//程序运行自动生成
    ├─ plugin			第三方类库、插件 //开发者手动创建，目录名称自定义
	├─ framework		框架源码目录
    	├─ src			框架核心类库
        	├─ cmd		框架命令类库
			├─ db		框架数据库类库
            └─ language	框架语言类
        ├─ cao.php		框架类文件
        └─ version.ini	框架版本号
	└─ index.php       	入口文件
~~~
为了安全起见，可将网站根目录入口指定单独的目录中，如下：
~~~
├─ application		应用模块目录	
├─ cache			运行缓存目录	
├─ plugin			第三方类库、插件
├─ framework		框架源码目录
└─ localhost		网站根目录 //开发者手动创建，目录名称自定义
	└─ index.php	入口文件
~~~