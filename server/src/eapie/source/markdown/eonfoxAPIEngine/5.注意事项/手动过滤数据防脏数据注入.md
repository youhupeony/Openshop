## 如果数据是由前端传入进来，而我们要对数据拼接处理，那么需要手动过滤数据
```
$data = cmd(array($data), 'str addslashes');
```
完整的示例代码（ request/shop/admin_goods_type.php::api_edit ）：
```
if( !empty($data['type_id']) && is_array($data['type_id'])){
    //清理数据
    $type_id = array();
    foreach($data['type_id'] as $value){
        if(is_string($value) || is_numeric($value)){
            //这里手动过滤一下
            $type_id[] = cmd(array($value), 'str addslashes');
        }
    }
}
		
if( !empty($type_id) ){
    //获取分类数据
    $in_string = "\"".implode("\",\"", $type_id)."\"";
    $type_where = array();
    //第三个参数是true，表示不加单引号并且强制不过滤，前面已经手动过滤了，这样就做到字符串拼接了
    $type_where[] = array("type_id IN([-])", $in_string, true);
    $type_where[] = array("[and] type_module=[+]", parent::MODULE_SHOP_GOODS_TYPE);
    //获取商品分类数据
    $type_data = object(parent::TABLE_TYPE)->select(array("where"=>$type_where));
}
```