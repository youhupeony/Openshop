## 购物流程图
![](./images/系统模块/购物商城/购物流程.png)

## 关于立即购买
立即购买也是走购物车流程。用户在点击立即购买的时候，前端程序会先请求添加到购物车的接口，注意`buy_now`请求参数设为1。然后返回值获得购物车ID，然后到结算页，请求结算接口。